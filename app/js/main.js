$(document).ready(function() {
    $( "#product" ).tabs();

    $(".size-name").click(function() {
        $(".size-name").each(function() {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
    });

    $(".product__tooltip").click(function() {
        $(this).toggleClass("toggle");
    });
    responsive();    
});

$(window).resize(function() {
    responsive();
});

function responsive() {
    if ($(window).width() < 768) {
        $("#grid").owlCarousel({
            items: 1,
            center: true,
            nav: true,
            dots: false,
            navText: ['', '']
        });
    }
    else {
        $("#grid").trigger('destroy.owl.carousel');
    }

    if($(window).width() > 768 && $(window).width() < 992) {
        let height = $(".product__title").height();
        $(".product__share").css("top", height+30+"px");
    }
}